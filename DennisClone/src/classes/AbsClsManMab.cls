/**
* 
*Abstract Class for Extended Controller
*
@author Shawn Liu
@created 2015-03-06
@version 1.0
@since 30.0
*
@changelog
* 2015-03-06 Shawn liu <shawn.liu@itbconsult.com>
* - Created
*/
public abstract class AbsClsManMab {
    //%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% -=BEGIN public members=- %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    /**
* manMap Wrapper which will be format by JSon and finally referenced by visualforce page
*/
    //Added transient by Bin Yuan 2015-11-02 due to fix view state
    public transient ClsManMapHierarchyUtil.ManMapWrapper wrapper = new ClsManMapHierarchyUtil.ManMapWrapper();
    /**
* list_externals is the collection of nodes available for Contact Nodes creation
*/
    //Added transient by Bin Yuan 2015-11-02 due to fix view state
    public transient list<ClsManMapHierarchyUtil.ContactWrapper> list_externals{get;set;}
    /**
* list_internals is the collection of nodes available for Influencers creation
*/
    //Added transient by Bin Yuan 2015-11-02 due to fix view state
    public transient list<ClsManMapHierarchyUtil.ContactWrapper> list_internals{get;set;}
    
    public set<Id> set_defaultIds = new set<Id>();
    
    public String qinfo{get;set;}
    public String qtype{get;set;}
    
    //%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% -=END public members=- %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    /**
* Used for retrieving the list_externals and list_internals
*
@author Shawn Liu
@created 2015-03-06
@version 1.0
@since 30.0
*
@Param maptype string  stands for Man Map Id
@changelog
* 2015-03-06 Shawn liu <shawn.liu@itbconsult.com>
* - Created
*/
    public void initialize(string maptype){
        list_externals = ClsManMapHierarchyUtil.getExternalContacts(maptype,wrapper);
        list_internals = ClsManMapHierarchyUtil.getInternalContacts(maptype,wrapper);
        postInitialize();
    }
    /**
* abstract method which need to override by subclasses and is used for do extra process after initialize
*
@author Shawn Liu
@created 2015-03-06
@version 1.0
@since 30.0
*
@changelog
* 2015-03-06 Shawn liu <shawn.liu@itbconsult.com>
* - Created
*/
    public abstract void postInitialize();
    /**
* search function for retrieving the externals or internals contact based on qtype and qinfo
*
@author Shawn Liu
@created 2015-03-06
@version 1.0
@since 30.0
*
@Param qinfo string  contact name input by end user
@Param qtype string  contact belonging to which type()
@changelog
* 2015-03-06 Shawn liu <shawn.liu@itbconsult.com>
* - Created
*/
    public void doSearch(){
        if(qtype.equals(ClsManMapHierarchyUtil.EXTERNAL)){
            list_externals = ClsManMapHierarchyUtil.doSearch(qinfo, qtype);
        }else{
            list_internals = ClsManMapHierarchyUtil.doSearch(qinfo, qtype);
        }
        postDoSearch();  
    }
    
    /**
* abstract method which need to override by subclasses and is used for do extra process after doSearch
*
@author Shawn Liu
@created 2015-03-19
@version 1.0
@since 30.0
*
@changelog
* 2015-03-19 Shawn liu <shawn.liu@itbconsult.com>
* - Created
*/
    public abstract void postDoSearch();
    /**
* abstract method which need to override by subclasses and is used for do used latest external contact id set
*
@author Shawn Liu
@created 2015-03-11
@version 1.0
@since 30.0
*
@changelog
* 2015-03-11 Shawn liu <shawn.liu@itbconsult.com>
* - Created
*/
    public abstract set<Id> getNodeContactsOnManMapId();
}